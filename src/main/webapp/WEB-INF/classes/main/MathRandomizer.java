package main;

import java.util.Random;

public class MathRandomizer implements ReelRandomizer {
    private int max;

    public MathRandomizer(int max) {
        this.max = max;
    }

    @Override
    public int getRandom() {
        return new Random().nextInt(max);
    }
}
