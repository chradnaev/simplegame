package main;

public class Utils {
    public static String joinMatrix(int[][] stopReel, Character separator, Character lineSeparator) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < stopReel.length; i++) {
            for (int j = 0; j < stopReel[i].length; j++) {
                result.append(stopReel[j][i]).append(separator);
            }
            result.deleteCharAt(result.length() - 1).append(lineSeparator);
        }
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    public static String joinArray(int[] array, Character separator) {
        StringBuilder result = new StringBuilder();
        for (int el : array) {
            result.append(el).append(separator);
        }
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    public static String joinArray(double[] array, Character separator) {
        StringBuilder result = new StringBuilder();
        for (double el : array) {
            result.append(el).append(separator);
        }
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }

    public static String joinArray(float[] array, Character separator) {
        StringBuilder result = new StringBuilder();
        for (double el : array) {
            result.append(el).append(separator);
        }
        result.deleteCharAt(result.length() - 1);
        return result.toString();
    }
}
