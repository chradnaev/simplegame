package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;


public class Game {
    private Logger logger = LogManager.getLogger(Game.class);
    private static Game instance = null;
    private int[] playLineCounts = {2, 3, 4, 5 };
    private int[] betsPerLines = { 1, 2, 3, 5, 10 };
    private final int window_size = 3; // размер окна в фиксированном барабане
    private final int initialCredits = 10;
    // распределение символов по барабанам
    private final int[][] reels = {
        {1, 11, 2, 10, 3, 11, 4, 10, 5, 6, 10, 7, 8, 10, 9, 10, 11, 5, 8, 9, 6, 7, 8, 10, 11},
        {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 4, 11, 9, 11, 7, 9, 11, 7, 11, 9, 6, 8, 9, 7},
        {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 9, 6, 10, 11, 9, 10, 11, 8, 11, 10, 11, 10, 8, 10}
    };
    // размеры выигрышей i - номер символа, j - число повторов
    private final int[][] wins = {
        {100, 30, 0},
        {80, 20, 0},
        {80, 20, 0},
        {50, 10, 0},
        {50, 10, 0},
        {25, 5, 0},
        {20, 5, 0},
        {20, 5, 0},
        {10, 5, 0},
        {10, 5, 0},
        {10, 5, 2}
    };
    // линии выигрыша
    private final int[][] lines = {
        {0, 2, 0},
        {0, 1, 0},
        {2, 1, 1},
        {2, 0, 2},
        {1, 2, 0}
    };
    private ReelRandomizer randomizer = new MathRandomizer(getReelLength());


    public static Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    protected Game() {
    }

    /**
     * Вход в игру, получение параметорв игры
     * @param credits
     * @return
     */
    public String enter(int credits) {
        StringBuffer result = new StringBuffer();
        result.append("playLineCounts=").append(Utils.joinArray(playLineCounts, '|'));
        result.append("&betsPerLines=").append(Utils.joinArray(betsPerLines, '|'));
        result.append("&reelCount=").append(reels.length);
        result.append("&credits=").append(credits);
        result.append("&windowSize=").append(window_size);
        result.append("&lines=");
        for (int[] line : lines) {
            result.append(Utils.joinArray(line,'|')).append('|');
        }
        result.deleteCharAt(result.length() - 1);

        return  result.toString();
    }

    /**
     * Подсчет выигрыша с учетом множителя на линию
     * @param state Начальное состояние игры
     * @return Результат игры с подсчитанным выигрышом с учетом множителя на линию
     */
    public void placeBet(GameState state) {
        computeBetPlace(state);

        int[] linePayoutWithCoeff = Arrays.stream(state.getLinePayouts()).map((val)->val * state.getBetPerLine()).toArray();
        state.setLinePayouts(linePayoutWithCoeff);

        int resultWin = state.getWin() * state.getBetPerLine();
        state.setWin(resultWin);
    }

    /**
     * Подсчет только чистого выигрыша без множителя на линию с вычетом из него числа линий
     * @param state начальное состояние игры
     * @return результат игры без величины кредитов
     */
    protected void computeBetPlace(GameState state) {
        int[][] stopReel = spinReels();
        int win = 0;
        int[] linePayouts = new int[state.getPlayLineCount()];
        log(Utils.joinMatrix(stopReel,'|','/'));

        for (int i =0; i < state.getPlayLineCount(); i++) {
            /* Вычисление выигрышей по линиям */
            int linePayout = countLinePayout(state, stopReel, lines[i]);

            win += linePayout;
            linePayouts[i] = linePayout;
        }

        log("Payout: "+Utils.joinArray(linePayouts, '|'));
        state.setLinePayouts(linePayouts);
        state.setStopReel(stopReel);
        state.setWin(win);
        state.setBetPlaced();
    }

    /**
     * Подсчет выигрыша по линии
     * @param stopReel  - положение барабана
     * @param line - линия выигрыша
     * @return - сумма выигрыша
     */
    protected int countLinePayout(GameState state, int[][] stopReel, int[] line) {
        Map<Integer, Integer> repeats = countSymbolRepeats(stopReel, line);

        /* Вычисление выигрышей по линиям */
        int linePayout = 0;
        for (Map.Entry<Integer, Integer> entry : repeats.entrySet()) {
            int symbol = entry.getKey();
            int symbolRepeat = entry.getValue();
            if (symbolRepeat != 0) {
                linePayout = wins[symbol - 1][reels.length - symbolRepeat];
            } else {
                linePayout = 0;
            }
            if (linePayout != 0) {
                state.addWinCombination(symbol, symbolRepeat, linePayout);
            }
        }

        return linePayout;
    }

    /**
     * Подсчет числа повторов символов по линии
     * @param stopReel положение барабана
     * @param winLine линия выигрыша
     * @return (номер символа) -> число повторов
     */
    protected Map<Integer, Integer> countSymbolRepeats(int[][] stopReel, int[] winLine) {
        Map<Integer, Integer> repeats = new HashMap<>();

        int symbol = stopReel[0][winLine[0]];
        int repeatCount = 1;
        for (int i = 1; i < winLine.length; i++) {
            if (stopReel[i][winLine[i]] != symbol) {
                break;
            } else {
                repeatCount++;
            }
        }
        repeats.put(symbol, repeatCount);
        log("win line:"+Arrays.toString(winLine));
        log("repeats: "+repeats);

        return repeats;
    }

    /**
     * Раскрутка барабана
     * @return положение барабана
     */
    protected int[][] spinReels() {
        int[][] stopReel = new int[reels.length][window_size];

        /* Раскрутка барабанов и запись их положений в stopReel */
        int reelIndex = 0;
        for (int i = 0; i < reels.length; i++) {
            int[] reel = reels[i];
            int pos = randomizer.getRandom();

            for (int j = 0; j < window_size; j++) {
                int nextPos = (pos + j) % reel.length;
                stopReel[reelIndex][j] = reel[nextPos];
            }
            reelIndex++;
        }
        log("\n"+Utils.joinMatrix(stopReel, ',', '\n'));
        return stopReel;
    }

    public int getInitialCredits() {
        return initialCredits;
    }
    private void log(String msg) {
        logger.trace(msg);
    }

    public void setRandomizer(ReelRandomizer randomizer) {
        this.randomizer = randomizer;
    }

    public int getReelCount() {
        return this.reels.length;
    }

    public int getReelLength() {
        return reels[0].length;
    }
}
