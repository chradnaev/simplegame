package main;

import java.util.LinkedList;
import java.util.List;

public class GameState {
    private int balance;
    private int win;
    private int[] linePayouts;
    private int[][] stopReel;
    private int betPerLine;
    private int playLineCount;
    private boolean betPlaced = false;
    private List<String> winCombinations = new LinkedList<>();

    public GameState(int balance) {
        this.balance = balance;
    }

    public String toParameterString() {
        StringBuilder result = new StringBuilder();

        result.append("payout=").append(Utils.joinArray(linePayouts, '|'));
        result.append("&stopreel=").append(Utils.joinMatrix(stopReel, '|','|'));
        result.append("&balance=").append(balance);
        result.append("&win=").append(win);

        return result.toString();
    }
    public void setBetPlaced() {
        this.betPlaced = true;
    }
    public boolean isBetPlaced() {
        return betPlaced;
    }

    public int getBetPerLine() {
        return betPerLine;
    }

    public void setBetPerLine(int betPerLine) {
        this.betPerLine = betPerLine;
    }

    public int getBalance() {
        return balance;
    }

    public void plusBalance(int balance) {
        if (balance < 0) {
            throw new IllegalArgumentException("Balance must me positive");
        }
        this.balance += balance;
    }
    public void minusBalance(int balance) {
        if (balance < 0) {
            throw new IllegalArgumentException("Balance must me positive");
        }
        this.balance -= balance;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int[] getLinePayouts() {
        return linePayouts;
    }

    public void setLinePayouts(int[] linePayouts) {
        this.linePayouts = linePayouts;
    }

    public int[][] getStopReel() {
        return stopReel;
    }

    public void setStopReel(int[][] stopReel) {
        this.stopReel = stopReel;
    }

    public int getPlayLineCount() {
        return playLineCount;
    }

    public void setPlayLineCount(int playLineCount) {
        this.playLineCount = playLineCount;
    }

    public void addWinCombination(int symbol, int repeat, int win) {
        winCombinations.add(symbol + ":" + repeat + ":" + win);
    }

    public List<String> getWinCombinations() {
        return winCombinations;
    }
}
