package main;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;


public class GameServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log("parameters: "+req.getParameterMap().toString());
        HttpSession session = req.getSession();
        Game game = Game.getInstance();
        int balance;
        if (session.getAttribute("balance") == null) {
            session.setAttribute("balance", game.getInitialCredits());
            balance = game.getInitialCredits();
        } else {
            balance = (int) session.getAttribute("balance");
        }
        GameState state = new GameState(balance);
        StringBuilder result = new StringBuilder();

        try {
            switch (req.getParameter("cmd")) {
                case "enter":
                    result.append(game.enter(balance));
                    break;
                case "placebet":
                    Map<String, String[]> params = req.getParameterMap();
                    int playLineCount = Integer.parseInt(params.get("playLineCount")[0]);
                    int betPerLine = Integer.parseInt(params.getOrDefault("betPerLine", new String[]{"0"})[0]);
                    state.setPlayLineCount(playLineCount);
                    state.setBetPerLine(betPerLine);

                    game.placeBet(state);
                    state.plusBalance(state.getWin());
                    state.minusBalance(playLineCount * betPerLine);

                    req.getSession().setAttribute("balance", state.getBalance());
                    result.append(state.toParameterString());
                    break;
                default:
                    throw new IllegalArgumentException("unknown command");
            }
        } catch (Exception exc) {
            log("error",exc);
            result = new StringBuilder("result=error");
        } finally {
            log(result.toString());
            resp.getWriter().write(result.toString());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }

    private Properties toProperties(HttpServletRequest req) {
        Properties props = new Properties();
        Enumeration<String> param = req.getParameterNames();
        while (param.hasMoreElements()) {
            String pname = param.nextElement();
            props.put(pname, req.getParameter(pname));
        }
        System.out.println(props);

        return props;
    }
}
