import Vue from 'vue'
import App from './App.vue'
import buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(buefy);

new Vue({
  el: '#app',
  render: h => h(App)
})
