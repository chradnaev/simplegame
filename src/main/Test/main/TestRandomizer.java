package main;

public interface TestRandomizer extends ReelRandomizer {
    int[] getVector();
    int nextIndex();
}
