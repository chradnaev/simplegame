package main;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ProtectedMethodTests extends Game {
    private Game game = Game.getInstance();

    @Test
    public void testSymbolCount() {
        int[][] stopReel = {{10, 11, 5}, {9, 11, 7}, {5, 6, 7}};

        int[] line = {0, 2, 0};
        Map<Integer, Integer> repeats = game.countSymbolRepeats(stopReel, line);
        Assert.assertTrue(repeats.getOrDefault(9, -1) == 1);
        Assert.assertTrue(repeats.getOrDefault(6, -1) == 1);
        Assert.assertTrue(repeats.getOrDefault(4, -1) == 1);

        line = new int[] {0, 1, 0};
        repeats = game.countSymbolRepeats(stopReel, line);
        Assert.assertTrue(repeats.getOrDefault(9, -1) == 1);
        Assert.assertTrue(repeats.getOrDefault(10, -1) == 1);
        Assert.assertTrue(repeats.getOrDefault(4, -1) == 1);
    }

    @Test
    public void testWinCount() {
        int[][] stopReel = {{10, 11, 5}, {9, 10, 7}, {5, 6, 7}};

        int[] line = {0, 2, 0};
        GameState state = new GameState(0);
        int win = game.countLinePayout(state, stopReel, line);
        Assert.assertEquals(win, 0);

        line = new int[] {0, 1, 0};
        win = game.countLinePayout(state, stopReel, line);
        Assert.assertEquals(win, 5);

        line = new int[] {0, 2, 2};
        win = game.countLinePayout(state, stopReel, line);
        Assert.assertEquals(win, 5);
    }
}
