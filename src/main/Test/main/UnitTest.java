package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;


public class UnitTest {
    private Logger logger = LogManager.getLogger(UnitTest.class);
    // Сортировка по числу повторов
    private Comparator<Map.Entry<String, Integer>> entryValueComparer = Comparator.comparing(Map.Entry::getValue);
    // Сортировка по величине выигрыша
    private Comparator<Map.Entry<String, Integer>> winComparer = Comparator.comparingInt((e)->Integer.parseInt(e.getKey().split(":")[2]));

    @Test
    public void testRandomStatistics() {
        int repeatCount = 100000;
        Game instance = Game.getInstance();
        ReelRandomizer randomizer = new MathRandomizer(instance.getReelLength());
        countStatistics(randomizer, repeatCount, winComparer);
    }
    @Test
    public void testSequenceRandomizer() {
        Game instance = Game.getInstance();
        int repeatCount = (int) Math.pow(instance.getReelLength(), instance.getReelCount());
        ReelRandomizer randomizer = new OrderedRandomizer(instance.getReelLength(), instance.getReelCount());
        countStatistics(randomizer, repeatCount, entryValueComparer);
    }

    /**
     * Подсчет статистики
     * @param randomizer класс для получения вектора положения барабана
     * @param repeatCount число повторов
     * @param comparer лямбда для сортировки результирующей мапы вида ("символ:дубли:выигрыш"=к-во попадений)
     */
    public void countStatistics(ReelRandomizer randomizer, int repeatCount, Comparator<Map.Entry<String, Integer>> comparer) {
        Game instance = Game.getInstance();
        instance.setRandomizer(randomizer);
        int lineCount = 3;
        int betPerLine = 1;
        double[] averageLinePayout = new double[lineCount];
        Arrays.fill(averageLinePayout, 0);
        int totalWin = 0;
        double averageComputeTime = 0;
        logger.info("repeat count: "+repeatCount);
        Map<String, Integer> stats = new TreeMap<>();

        for (int i=0; i < repeatCount; i++) {
            LocalTime now = LocalTime.now();
            GameState state = new GameState(0);
            state.setBetPerLine(betPerLine);
            state.setPlayLineCount(lineCount);
            instance.placeBet(state);
            addWinCombination(stats, state);

            for (int j = 0; j < state.getLinePayouts().length; j++) {
                averageLinePayout[j] += state.getLinePayouts()[j];
            }
            totalWin += state.getWin();
            averageComputeTime += Duration.between(now, LocalTime.now()).toMillis();
        }

        double RTP = (double) totalWin / (double) (repeatCount * betPerLine * lineCount);
        averageComputeTime /= repeatCount;
        int winCombinationCount = stats.values().stream().reduce((v1,v2)->v1+v2).get();

        logger.info(String.format("%s %d (%.2f%%)","Выигрышные комбинации: ",winCombinationCount,
            (double) winCombinationCount / (double) (repeatCount * lineCount) * 100));

        logger.info("Average win per line:");
        logger.info(Arrays.toString(Arrays.stream(averageLinePayout).map((el) -> el / repeatCount).toArray()));
        logger.info("RTP: " + RTP);
        logger.info("Average compute time (ms): " + averageComputeTime);
        logger.info("win combinations(symbol:repeats:win=statistics): ");
        System.out.println("\n");
        for (Map.Entry<String, Integer> entry : stats.entrySet().stream().sorted(comparer).collect(Collectors.toList())) {
            double percent = (double)entry.getValue() / (double) winCombinationCount * 100;
            System.out.println(String.format("%15s (%.2f%%):%s", entry.getKey()+"="+entry.getValue(),
                percent, repeatedCharacter('#', Math.round((float) percent))));
        }
    }

    @Test
    public void testTester() {
        OrderedRandomizer randomizer = new OrderedRandomizer(10,3);
        for (int i = 0; i < 1000; i++) {
            System.out.print(i + ": ");
            for (int j=0; j < 3; j++) {
                System.out.print(randomizer.getRandom()+" ");
            }
            System.out.print("\n");
        }
    }

    private void addWinCombination(Map<String, Integer> map, GameState state) {
        for (String winCombination : state.getWinCombinations()) {
            if (map.containsKey(winCombination)) {
                map.replace(winCombination, map.get(winCombination) + 1);
            } else {
                map.put(winCombination, 1);
            }
        }
    }

    private static String repeatedCharacter(Character st, int count) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < count; i++) {
            result.append(st);
        }

        return result.toString();
    }
    @Test
    public void t() {
        System.out.println(OrderedRandomizer.pow(2, 5));
    }
}
