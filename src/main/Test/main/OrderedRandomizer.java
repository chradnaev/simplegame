package main;


public class OrderedRandomizer implements ReelRandomizer {
    private int max;
    private int size;
    private int valueCounter = 0;
    private int index;

    public OrderedRandomizer(int max, int size) {
        this.max = max;
        this.size =size;
        this.index = 0;
    }

    @Override
    public int getRandom() {
        int value = (valueCounter / (int) Math.pow(max, index)) % max;
        index = (index + 1) % size;
        if (index == 0) {
            valueCounter++;
        }
        return value;
    }
    public static int pow(int value, int degree) {
        int result = value;
        while (degree > 1) {
            if ((degree & 1) == 1) {
                result *= value;
                degree--;
            } else {
                result *= result;
                degree /= 2;
            }
        }
        return result;
    }
}
