Game protocol

servlet name = game

3 reels, 25 fixed lines

Icons: 
    P1	Star
    
    P2	Seven
    
    P3	Watermelon
    
    P4	Grape
    
    P5	Strawberry
    
    P6	Bell
    
    P7	Plum
    
    P8	Pear
    
    P9	Orange
    
    P10	Lemon
    
    P11	Cherry


Payouts:

    Three	Two	One
    
    100	30	0
    
    80	20	0
    
    80	20	0
    
    50	10	0
    
    50	10	0
    
    25	5	0
    
    20	5	0
    
    20	5	0
    
    10	5	0
    
    10	5	0
    
    10	5	2
    

Line descriptions:

    0, 2, 0; // 1
    
    0, 1, 0; // 2
    
    2, 1, 1; // 3
    
    2, 0, 2; // 4
    
    1, 2, 0; // 5

GAME ENTERING
request:

    cmd=enter
response:

    betsPerLines=1|2|3|5|10&
    
    reelCount=3&
    
    credits=10&
    
    balance=1008.0&

    lines=0|2|0|0|1|0|2|1|1|2|0|2|1|2|0&
    
    playLineCounts=2|3|4|5&
    
    windowSize=3&
    
    result=ok


PLACING BET
request:

    cmd=placebet&
    
    betPerLine=5.0&
    
    playLineCount=5
    
response:

    payout=0|0|0|5|2&
    
    stopreel=4|10|5|7|11|9|5|6|7&
    
    win=0.0&
    
    balance=1008.0&
    
    result=ok